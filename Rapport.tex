\documentclass[a4paper, 12pt]{article}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[francais]{babel}
\usepackage[hidelinks]{hyperref}
\usepackage{layout}
\usepackage{graphicx}
\usepackage{fancyhdr}
\usepackage{geometry}
\usepackage{lastpage}
\usepackage{csquotes}
\usepackage{caption}
\geometry{hmargin=2.5cm,vmargin= 3cm}
\pagestyle{fancy}


\author{Corentin Coirre} 
\title{Word embeddings: étude diachronique de la dérive sémantique des mots} 
\date{\today}
\renewcommand\headrulewidth{0.5pt}
\fancyhead[L]{Université Lille 3}
\fancyhead[R]{Corentin Coirre}
\fancyfoot[C]{\thepage/\pageref{LastPage}}
\fancyfoot[L]{
\includegraphics[scale=0.10]{logo.jpg}}
\begin{document}
\maketitle
\thispagestyle{empty}
\pagebreak
\tableofcontents
\pagebreak
\section*{Introduction}
\addcontentsline{toc}{section}{Introduction}

Ce document a pour vocation de présenter la notion de word embeddings et son fonctionnement et d'introduire l'application à la dérive sémantique des mots. En effet, dans le cadre de ma licence j'ai la possibilité de réaliser un travail d'étude et de recherche (TER), et j'ai choisi un sujet dans l'apprentissage automatique car c'est un domaine qui m'intéresse énormément.

L'analyse textuelle n'est pas une discipline récente, cependant avant l'arrivée des word embbedings on disposait seulement d'une approche statistique (loi de Zipf par exemple). De plus on s'est aperçu qu'il était difficile d'assimiler une signification à un mot car certains peuvent en avoir plusieurs et la signification peut dépendre de phrase ou même du contexte dans lequel il est énoncé. Ainsi pour palier à différentes problématiques comme la traduction ou la reconnaissance vocale l'homme a dû développer des outils afin d'analyser la sémantique d'un mot ou même d'une association de mot. C’est ainsi que l'on a vu émerger la technique de word embedding. Le word embedding est un terme utilisé depuis le début du XXIème siècle, il provient du machine learning qui connait une fulgurante progression durant cette dernière décennie. On en parle de plus en plus depuis les récents travaux de Tomas Mikolov et la présentation de son algorithme Word2Vec en 2013

Grâce à cette innovation nous allons étudier la dérive sémantique des mots dans le temps. En effet le même mot utilisé aujourd'hui ou le siècle dernier peut avoir une signification différente. Une étude a déjà été réalisé sur ce sujet en 2016 par le département informatique de l'université de Stanford, le but à l'issue de ce TER sera de reproduire certains résultats de cet étude.

Dans un premier temps je vais présenter le concept général du word embedding et expliquer synthétiquement comment fonctionnent les principaux algorithmes utilisés. Ensuite je vais parler de l’intérêt et du word embeddings et mentionner des exemples de champ d'application. Enfin je vais montrer ce que le word embedding apporte à l'analyse de la dérive sémantique et présenter les principaux résultats.

\pagebreak
\section{Concept et fonctionnement}
\subsection{Concept et définition}

Tout d'abord voyons la définition de word embedding du site Wikipédia:
\begin{displayquote}
\textit{''Le word embedding (« plongement de mots » ou « plongement lexical » en français) est une méthode se focalisant sur l'apprentissage d'une représentation de mots. Cette technique permet de représenter chaque mot d'un dictionnaire par un vecteur de nombres réels correspondant''}
\end{displayquote}
Comme l’explicite cette définition, le word embedding est une technique utilisée pour réaliser une représentation des mots d'un dictionnaire dans un espace vectoriel afin d’obtenir une lecture facilitée du lien sémantique entre ces mots.

On utilise donc différents algorithmes d'apprentissage qui permettent d'obtenir une représentation de chaque mot sous forme d'un vecteur de plusieurs valeurs réels. Un mot est en fait défini par son contexte. En effet les mots qui sont régulièrement associés à un mot en particulier (proche du mot en question dans la phrases) définissent celui-ci. Pour illustrer cela par un exemple, on peut considérer le mot ''chat'', qui dans un corpus sera souvent associé au mot ''manger'' ou ''dormir''. Alors le mot ''chien'' qui est aussi associé à ces mêmes mots aura une représentation vectorielle proche de celle du mot ''chat'' car ils désignent deux choses qui se ressemble mais ne sont pas tout à fait similaire. Notons bien que ce procédé ne permet pas d'identifier de lien entre les mots souvent utilisé ensemble mais plutôt d'identifier les mots qui sont utilisés dans le même contexte.




\subsection{fonctionnement des principaux algorithmes}


Rappelons tout d'abord que le word embedding est une méthode basée sur l'apprentissage automatique et qu'il faut donc des données sous forme de texte que l'on appelle corpus. Notre corpus contient donc un vocabulaire qui représente tous les mots qui y sont utilisés. Avant l'exécution des différents algorithmes on transforme le corpus. Pour les outils de traitement de texte on utilise la notion de token qui ressemble à la notion de mot. Un token est défini comme une suites de caractère contenu entre deux signes de ponctuation (espace, apostrophe, point...). On procède également à la ''lemmatisation'' qui consiste à regrouper tous les mots d'une même famille en une seul suite de caractère, la lemme (ou forme canonique). Par exemple si on rencontre le verbe ''manger'' il sera lemmatisé sous sa forme infinitive ''manger'' quel que soit la conjugaison sous laquelle il est rencontré dans le texte.

Le processus peut être divisé en deux étapes, la première est la phase d’apprentissage. Comme mentionné précédemment on utilise le contexte dans lequel le mot est employé pour expliquer sa sémantique. Le contexte d'un mot est en fait défini par les lemmes voisins de notre mot dans le corpus sur lequel on réalise l'apprentissage. On peut choisir le nombre de tokens voisins que l'on va utiliser avant et après notre mot lors de la phase d’apprentissage. Les méthodes classiques (par opposition aux méthodes utilisant les réseaux neuronaux) basent leurs calcules sur une matrice contenant le vocabulaire en ligne et le contexte en colonnes. Le problème est que l'on obtient une matrice creuse, c'est à dire contenant une majorité de zéro donc la lecture des données est difficile et les calculs sur ordinateur peuvent être très long. Pour pallier à ce problème on réalise une décomposition en valeur singulières c'est à dire que l'on réduit la taille de la matrice tout en conservant un maximum d'information. Les algorithmes basés sur les réseaux neuronaux sont plus complexes à analyser, word2vec qui a été créé en 2013 est le plus connue. La mise en place de ces algorithmes est plus couteuse en ressources informatiques mais on obtient la majorité du temps de meilleurs résultats. 

La deuxième étape est l’analyse, c’est en fait la lecture de ce qu’on a produit lors de la première étape produite précédemment. Quel que soit l'algorithme que l'on utilise la manière d'analyser le résultat de l'apprentissage est la même. Chaque mot est représenté par un vecteur de valeur réels. Plus ces vecteurs sont identiques et plus la sémantique des deux mots sera proche et inversement si les vecteurs sont orthogonaux alors ils auront une sémantique très différente. Le word embedding permet également d'analyser le lien entre plusieurs mots ou groupe de mots. Si un lien existe entre le mot a et le mot b on peut par apprentissage trouver d'autre mots qui ont le même lien que b avec le mot a. Par exemple on peut identifier le lien entre les mot ''France'' et ''Marseille'' et demander un mot qui a le même lien avec ''France'', on trouvera très certainement ''Paris'' ou bien une autre ville de France. On peut même réaliser un raisonnement analogue de ce lien avec d’autre mots. Par exemple on peut chercher un mot qui a le même lien avec ''Espagne'' que ''Marseille'' avec le mot ''France''.
\vskip1cm
Nous allons voir dans la prochaine partie que l'apport du word embedding dans certains domaines ainsi que les principaux résultat que l'on a peut obtenir grâce à cela.
\pagebreak
\section{Intérêt et applications}

\subsection{Intérêt}

Comme nous avons pu le voir précédemment, le word embedding permet d’étudier la sémantique d'un mot ou d'un groupe de mot grâce à son contexte. L'algorithme peu donc comprendre les différents sens d'un mot et même les réutiliser correctement en fonction du contexte. Mais le word embedding peut aller plus loin car il permet de faire une analogie entre certain mots et expression. Ces deux possibilités amené par le word embedding sont à la base d’un grand nombre domaine qui utilisent la compréhension texte.

\subsection{Traduction}

Aujourd'hui les traducteurs sont basés sur des techniques d'appariement statistique. Avec l'arrivé du machine learning et du word embedding, on peut maintenant traduire des phrases entières avec une plus grande précision. Cependant, ces algorithmes demandent une grande puissance de calcul et donc cette technique n'est pas encore démocratisée car peu transportable. 

La traduction nécessite deux algorithmes et donc un apprentissage pour ces deux algorithmes. En effet un premier algorithme permet de comprendre le sens de la phrase dans la langue que l'on veut traduire. Il faut un deuxième algorithme qui permet de générer une phrase dans la langue voulue à partir du sens que l'on obtient à la fin du premier algorithme.

\subsection{Reconnaissance vocale}

La reconnaissance vocale qui est aujourd'hui très largement utilisé par de nombreuse institution est une application du word embedding. C'est en effet grâce à son développement et celui du machine learning que l'on a pu développer la reconnaissance vocale car c'est un domaine qui nécessite beaucoup de données et donc beaucoup de ressource informatique.

On utilise également deux algorithmes de word embedding pour traiter le problème de reconnaissance vocale. Le premier permet de déterminer à l'aide des mots déjà reconnues une probabilité sur quel mot pourrait suivre dans la phrase. Le second permet de remplacer le mot reconnu par ceux qui sont le plus proches phonétiquement du vocabulaire du corpus d'entrainement.

\subsection{Réponse automatique}

La réponse automatique à une question posée par l'utilisateur est un nouveau champ qui a amené à être développer avec l'arrivé du word embedding. Ici l'apport du word embedding est sa grande transportabilité, on peut en effet entrainer notre algorithme sur n'importe quel corpus pour qu'il soit capable de répondre à nos questions. 

Grâce à l'apport du word embedding, l'ordinateur est maintenant capable de faire l'analogie entre une question et sa réponse. En effet quand on entraine un algorithme de word embedding sur un corpus, celui-ci enregistre les liens entre les mots de ce corpus. Quand l'utilisateur pose une question alors l'algorithme vas lui répondre le mot ou groupe de mot en rapport avec les mots utilisés dans la question.

\vskip1cm

Nous avons donc vu en surface que le word embedding est utile dans bien des domaines et nous allons maintenant voir comment il est utilisé dans la dérive sémantique et quels sont les principaux résultats obtenu aujourd'hui.

\pagebreak
\section{Word embeddings et dérive sémantique}

\subsection{Méthode}

La méthode afin de réaliser une étude dans le temps est de répéter la phase d'apprentissage à chaque nouveau corpus écrit à différentes époques. De cette façon on obtient un vecteur contexte pour chaque mot et à chaque époque et on peut donc comparer les mots les plus proches du mot que l’on souhaite étudier à chaque époque.

Cependant il peut également être intéressant de quantifier l’importance du changement sémantique d’une époque à l’autre afin de remarquer des tendances lors de la dérive sémantique. On peut par exemple utiliser la corrélation de Spearman afin de mesurer la similarité entre le même mots à deux époques différentes. De cette façon on obtient une mesure du changement du contexte d’utilisation du mot que l’on peut comparer avec la fréquence d’utilisation de ce mot ou d’autre mesure. Cependant cette technique à des limites car les mots qui forment le contexte du mot étudié ont pu évoluer eux aussi.



\subsection{Principaux résultats}

Après l’apprentissage de notre corpus nous pouvons donc observer les vecteurs de réels établis pour chaque mot et les comparer entre eux. Cela nous permet d'observer l'évolution des mots de notre corpus. On peut observer ci-dessous une illustration de quelque mots anglais ayant subi une dérive sémantique dans le temps.

\begin{center}
\includegraphics[scale= 0.5]{wordpaths.png}
\captionof{figure}{exemple de dérive sémantique de 3 mots}
\end{center}
Dans le cas b on remarque que l'on a réalisé un apprentissage sur 3 corpus a 3 époques différentes. On peut interprété cette illustration de la manière suivante : Le mot ''Broadcast'' est utilisé avec ''seed'' ou ''sow'' car ce mot était utilisé comme ''semer'' est utilisé de nos jour en français. De nos jour ce mot est utilisé pour la transmission d'information ou de signal, c'est pour cela qu'il est plus proche des mots ''newspaper'' ou ''Radio''.

Ce n'est pas tout, les précédentes études ont permis d'apporter deux Lois importantes. La première, la loi de conformité (''the law of conformity'' en anglais) dit que les mots qui sont utilisés plus fréquemment sont moins touché par la dérive sémantique, la réciproque est vrai aussi. La seconde loi, la loi d'innovation (''Law of innovation'' en anglais) montre que les mots polysémiques changent plus souvent de sens.

\pagebreak
\section*{Conclusion}
\addcontentsline{toc}{section}{Conclusion}

Le word embedding est donc une véritable avancé pour l'étude textuel en particularité pour les études sémantique. Ce domaine n'est qu'au début de son existence et il reste une énorme marge de progression. Ses applications sont nombreuses et variés comme nous avons pu le voir. Il est à la base de ce qu'on appelle les chatbots que l'on va rencontrer de plus en plus et qui vas extrêmement faciliter un grand nombre d'aspect de la vie quotidienne.

Son application à l'étude diachronique de la dérive sémantique des mots permet d'obtenir d'intéressant résultats sur l'évolution qu'il y a pu avoir sur certains mots. Cela a également permis de remarquer des tendances générales sur l’évolution des mots dans le temps. En continuant la recherche dans ce sens nous pourrions nous attendre à pouvoir trouver de nouvelle loi sur la dérive sémantique des mots avec la possibilité de prédire avec précision dans quel sens la sémantique d'un mot pourrait-elle évoluer.

Cependant le machine learning est une discipline très récente et son utilisation au sein des algorithmes de word embedding n’est que superficiel. En effet, l’apprentissage profond (ou deep learning) qui est utilisé pour l’analyse d’image, n’est pas utiliser dans les algorithmes de word embedding. 


\pagebreak
\section*{Références}
\addcontentsline{toc}{section}{Références}

\begin{itemize}
\item \url{http://www.aclweb.org/anthology/Q15-1016}
\vskip0.5cm
\item \url{https://nlp.stanford.edu/projects/histwords/}
\vskip0.5cm
\item \url{https://code.google.com/archive/p/word2vec/}
\vskip0.5cm
\item \url{https://fr.wikipedia.org/wiki/Word_embedding}
\vskip0.5cm
\item \url{https://www.analyticsvidhya.com/blog/2017/06/word-embeddings-count-word2veec/}
\vskip0.5cm
\item \url{http://scoms.hypotheses.org/657}
\vskip0.5cm
\item \url{https://www.youtube.com/watch?v=RgUcQceqC_Y} 37'43s
\vskip0.5cm
\item \url{https://arxiv.org/pdf/1605.09096.pdf}

\end{itemize}

\end{document}