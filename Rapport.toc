\select@language {french}
\contentsline {section}{Introduction}{3}{section*.2}
\contentsline {section}{\numberline {1}Concept et fonctionnement}{4}{section.1}
\contentsline {subsection}{\numberline {1.1}Concept et d\IeC {\'e}finition}{4}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2}fonctionnement des principaux algorithmes}{4}{subsection.1.2}
\contentsline {section}{\numberline {2}Int\IeC {\'e}r\IeC {\^e}t et applications}{6}{section.2}
\contentsline {subsection}{\numberline {2.1}Int\IeC {\'e}r\IeC {\^e}t}{6}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Traduction}{6}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}Reconnaissance vocale}{6}{subsection.2.3}
\contentsline {subsection}{\numberline {2.4}R\IeC {\'e}ponse automatique}{6}{subsection.2.4}
\contentsline {section}{\numberline {3}Word embeddings et d\IeC {\'e}rive s\IeC {\'e}mantique}{8}{section.3}
\contentsline {subsection}{\numberline {3.1}M\IeC {\'e}thode}{8}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Principaux r\IeC {\'e}sultats}{8}{subsection.3.2}
\contentsline {section}{Conclusion}{9}{section*.3}
\contentsline {section}{R\IeC {\'e}f\IeC {\'e}rences}{10}{section*.4}
